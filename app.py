import os
import urllib
from time import sleep
import py_eureka_client.eureka_client as eureka_client

default_codes = [
        {"id":"1","name":"Assinatura do paciente no cupom vinculado não conforme documento oficial"},
        {"id":"2","name":"Ausência da cópia do documento do procurador"},
        {"id":"3","name":"Assinatura do procurador no cupom vinculado não conforme o documento oficial"},
        {"id":"4","name":"Ausência de endereço da instituição de atendimento na reita médica"},
        {"id":"5","name":"Ausência da assinatura do médico"},
        {"id":"6","name":"Ausência da assinatura no cupom vinculado"},
        {"id":"7","name":"Ausência da data de emissão na receita médica"},
        {"id":"8","name":"Ausência da documentação do paciente (CPF e/ou documento oficial com foto)"},
        {"id":"9","name":"Ausência da documentação do responsável (menor)"},
        {"id":"10","name":"Ausência da receita médica"},
        {"id":"11","name":"Ausência de endereço do paciente na receita médica (no ato da dispensação do medicamento)"},
        {"id":"12","name":"Ausência de endereço do paciente no cupom vinculado (no ato da dispensação do medicamento)"},
        {"id":"13","name":"Ausência de toda documentação solicitada"},
        {"id":"14","name":"Ausência do cupom fiscal"},
        {"id":"15","name":"Ausência do cupom vinculado"},
        {"id":"16","name":"Ausência do CRM e nome do médico"},
        {"id":"17","name":"Ausência do nome do paciente na receita médica"},
        {"id":"18","name":"Ausência de autenticação da procuração em cartório"},
        {"id":"19","name":"Cupom fiscal ilegível"},
        {"id":"20","name":"Cupom vinculado ilegível"},
        {"id":"21","name":"Receita médica ilegível"},
        {"id":"22","name":"Cópia dos documentos do paciente ilegíveis"},
        {"id":"23","name":"Receita medica com nome ou CRM do médico ilegível"},
        {"id":"24","name":"Receita médica com CRM do médico diferente do apresentado no sistema autorizador de vendas"},
        {"id":"25","name":"Cupom vinculado não possui relação com o cupom fiscal"},
        {"id":"26","name":"Receita medica com data de emissão vencida"},
        {"id":"27","name":"Cupom vinculado assinado, porém o paciente não é alfabetizado"},
        {"id":"28","name":"Procuração ilegível"},
        {"id":"29","name":"Receita médica com data posterior ao dia da autorização"},
        {"id":"30","name":"Receita medica rasurada"},
        {"id":"31","name":"Procuração incompleta"},
        {"id":"32","name":"Documentação do paciente incompleta"},
        {"id":"33","name":"Cupom fiscal incompleta"},
        {"id":"34","name":"Cupom vinculado incompleta"},
        {"id":"35","name":"Documentação do procurador incompleta"},
        {"id":"36","name":"Receita médica com Data de emissão ilegível"},
        {"id":"37","name":"Receita médica com endereço do paciente ilegível"},
        {"id":"38","name":"Cupom vinculado assinado pelo paciente menor de idade"},
        {"id":"39","name":"Documento do paciente diverge do apresentado no cupom vinculado"},
        {"id":"40","name":"Cupom vinculado com endereço do paciente divergente do apresentado na receita"},
        {"id":"41","name":"Receita médica com o nome do paciente ilegível"},
        {"id":"42","name":"Dispensado medicamento sem procuração"},
        {"id":"43","name":"Dispensado medicamento não prescrito na receita médica"},
        {"id":"44","name":"Dispensado fralda geriátrica para paciente com idade inferior a 60 anos, ou ser pessoa sem deficiência"},
        {"id":"45","name":"Intercambialidade incorreta do medicamento X"},
        {"id":"46","name":"Troca de dosagem do medicamento X"},
        {"id":"47","name":"Ausencia da posologia do medicamento X"},
        {"id":"48","name":"Procuração com data posterior à dispensação do medicamento"},
        {"id":"49","name":"Ausência da dosagem do medicamento na receita médica (mg/ml)"},
        {"id":"50","name":"Dispensado medicamento X em quantidade superior ao prescrito na receita médica"},
        {"id":"51","name":"Dispensado medicamento X em quantidade superior ao permitido pelo programa"},
        {"id":"52","name":"Ausência de impressão digital ou assinatura do paciente/outorgante na procuração"},
        {"id":"53","name":"Informações do cupom vinculado divergem da apresentada no sistema autorizador de vendas"},
        {"id":"54","name":"Outras"}
        ]


# The flowing code will register your server to eureka server and also start to send heartbeat every 30 seconds
def registry():
    # no spaces or underscores, this needs to be url-friendly
    service_name = 'elucidation'

    # This needs to be an IP accessible by anyone that
    # may want to discover, connect and/or use your service.
    flask_port = 5000
    ip = 'elucidation' #'127.0.0.1'
    your_rest_server_port = 5000

    home_page_url = 'manage/info'
    manage_health_path = 'manage/health'
    eureka_url = 'http://discovery:8761/eureka'

    try:
        # The flowing code will register your server to eureka server and also start to send heartbeat every 30 seconds
        registry_client, discovery_client = eureka_client.init(eureka_server=eureka_url,
                                                               app_name=service_name,
                                                               instance_ip=ip,
                                                               instance_port=your_rest_server_port)

        registry_client.send_heart_beat()
        # discovery_client.start()
    except Exception as e:
        sleep(30)
        return registry()


registry()


import json
from flask import Flask, send_from_directory, request
from pymongo import MongoClient, ReturnDocument
from bson import ObjectId

# set the project root directory as the static folder, you can set others.
app = Flask(__name__)

# Init mongodb client
client = MongoClient('mongodb://mongodb:27017/')  # 'mongodb://mongodb:27017/'
db = client.paragraph_db  # Create or get database with name paragraph_db


@app.route('/manage/health')
def manage_health():
    return json.dumps({'status': 'UP'})


@app.route('/manage/info')
def manage_info():
    return json.dumps({'app': 'elucidation'})


'''
    Elucidations CRUD endpoints
'''
@app.route('/elucidations', methods=['POST', 'GET'])
def elucidations():
    collection = db.elucidation
    if request.method == 'POST':
        elucidation = request.get_json()
        occ_id = collection.insert_one(elucidation).inserted_id
        return JSONEncoder().encode(elucidation)
    elif request.method == 'GET':
        elucidations = list(collection.find())
        return JSONEncoder().encode(elucidations)


@app.route('/elucidations/<id>', methods=['PUT', 'DELETE', 'GET'])
def elucidation(id):
    _id = ObjectId(id)
    collection = db.elucidation
    elucidation = collection.find_one({'_id': _id})
    if request.method == 'GET':
        return JSONEncoder().encode(elucidation)

    if request.method == 'PUT':
        new_elucidation = request.get_json()
        new_occ = collection.find_one_and_update(
                {'_id': _id},
                { '$set': new_elucidation },
                return_document=ReturnDocument.AFTER)
        return JSONEncoder().encode(new_occ)

    elif request.method == 'DELETE':
        del_occ = collection.find_one_and_delete({'_id': _id})
        return JSONEncoder().encode(del_occ)

@app.route('/elucidations/<id>/body')
def elucidation_body(id):
    _id = ObjectId(id)
    elucidation = db.elucidation.find_one({'_id': _id})
    registered_occs = list(map(lambda auth: list(map(lambda occ: occ['id'], auth['occurrences']))
            , elucidation['authorizations']))
    grouped_auths = list(map(lambda occ: 
            list(filter(lambda auth: list(map(lambda _occ: _occ['id'], auth['occurrences'])) == occ, elucidation['authorizations']))
            , registered_occs))
    formatted_auth = list(map(lambda auths: __format_auths(auths[1], registered_occs[auths[0]])
            , enumerate(grouped_auths)))
    formatted_auth = list(dict.fromkeys(formatted_auth))
    body = {
            'elucidation': elucidation,
            'body': formatted_auth
            }
    return JSONEncoder().encode(body)

def __format_auths(auths, occurrence_ids):
    occs = list(db.occurrence_code.find({ 'id': { '$in': occurrence_ids } }))
    auth_codes = ''
    if ('medicamento X' not in ' '.join(map(lambda o: o['name'], occs))):
        auth_codes = '; '.join(['({date}) {id}'.format(**a) for a in auths])
    for occ in occs:
        if ('medicamento X' in occ['name']):
            format_str =  'medicamento: '
            auths.sort(key=lambda a: list(filter(lambda o: o['id'] == occ['id'], a['occurrences']))[0]['remedy'])
            for (i, auth) in enumerate(auths):
                auth_occ = list(filter(lambda o: o['id'] == occ['id'], auth['occurrences']))[0]
                if (i == 0):
                    format_str +=  '{} ({}) {}'.format(auth_occ['remedy'], auth['date'], auth['id'])
                else:
                    if (auth_occ['remedy'] not in format_str):
                        format_str +=  '{} ({}) {}'.format(auth_occ['remedy'], auth['date'], auth['id'])
                    elif (auth['date'] not in format_str):
                        format_str +=  '({}) {}'.format(auth['date'], auth['id'])
                    else:
                        format_str +=  '{}'.format(auth['id'])
                if (i != len(auths)-1):
                    format_str += ', '

            occ['name'] = occ['name'].replace('medicamento X', format_str)
    description = '; '.join(map(lambda o: o['name'], occs))
    return '{}{};'.format(description, auth_codes)


'''
    Occurrrence Code CRUD endpoints
'''
@app.route('/occurrence_codes', methods=['POST', 'GET'])
def occurrence_codes():
    collection = db.occurrence_code
    if request.method == 'POST':
        occurrence_code = request.get_json()
        occ_id = collection.insert_one(occurrence_code).inserted_id
        return JSONEncoder().encode(occurrence_code)
    elif request.method == 'GET':
        occurrence_codes = list(collection.find())
        if (not occurrence_codes):
            collection.insert_many(default_codes)
            occurrence_codes = list(collection.find())
        return JSONEncoder().encode(occurrence_codes)


@app.route('/occurrence_codes/renew', methods=['DELETE'])
def occurrence_codes_renew():
    collection = db.occurrence_code
    collection.delete_many({})
    collection.insert_many(default_codes)
    return JSONEncoder().encode(list(collection.find()))


@app.route('/occurrence_codes/<id>', methods=['PUT', 'DELETE', 'GET'])
def occurrence_code(id):
    _id = ObjectId(id)
    collection = db.occurrence_code
    occurrence_code = collection.find_one({'_id': _id})
    if request.method == 'GET':
        return JSONEncoder().encode(occurrence_code)

    if (request.method == 'PUT'):
        new_occurrence_code = request.get_json()
        new_occ = collection.find_one_and_update(
                {'_id': _id},
                { '$set': new_occurrence_code },
                return_document=ReturnDocument.AFTER)
        return JSONEncoder().encode(new_occ)

    elif (request.method == 'DELETE'):
        del_occ = collection.find_one_and_delete({'_id': _id})
        return JSONEncoder().encode(del_occ)


'''
    Authorizations save and list endpoints
'''
@app.route('/authorizations', methods=['POST', 'GET'])
def authorizations():
    collection = db.authorizations
    if (request.method == 'POST'):
        authorizations = request.get_json()
        occ_id = collection.insert_one(authorizations).inserted_id

    authorizations = list(collection.find())
    if (not authorizations):
        authorizations = []
    return JSONEncoder().encode(authorizations)


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)

